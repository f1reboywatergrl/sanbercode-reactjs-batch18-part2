import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas9 from '../tugas-9/Tugas9.js';
import Tugas10 from '../tugas-10/Tugas10.js';
import Tugas11 from '../tugas-11/Tugas11.js';
import Tugas12 from '../tugas-12/Tugas12.js';
import Tugas13 from '../tugas-13/Tugas13.js';
import Tugas14 from '../tugas-14/control.js';

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/Tugas9">Tugas9</Link>
            </li>
            <li>
              <Link to="/Tugas10">Tugas10</Link>
            </li>
            <li>
              <Link to="/Tugas11">Tugas11</Link>
            </li>
            <li>
              <Link to="/Tugas12">Tugas12</Link>
            </li>
            <li>
              <Link to="/Tugas13">Tugas13</Link>
            </li>
            <li>
              <Link to="/Tugas14">Tugas14</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/Tugas9">
            <Tugas9 />
          </Route>
          <Route path="/Tugas10">
            <Tugas10 />
          </Route>
          <Route path="/Tugas11">
            <Tugas11 />
          </Route>
          <Route path="/Tugas12">
            <Tugas12 />
          </Route>
          <Route path="/Tugas13">
            <Tugas13 />
          </Route>
          <Route path="/Tugas14">
            <Tugas14 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
