import React, {Component} from 'react';

class Tugas11 extends React.Component{
        constructor(props){
        super(props)
        this.state = {
          time: 100
        }
      }
    componentDidMount(){
        if (this.props.start !== undefined){
          this.setState({time: this.props.start})
        }
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
      }
    componentWillUnmount(){
        clearInterval(this.timerID);
    }    
    tick() {
        this.setState({
          time: this.state.time - 1 
        });
    }
    runclock(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        var time_notation = "AM";
        if(hours>12){
            hours-=12;
            time_notation="PM";
        }
        return (
            <a>{`${hours}:${mins}:${secs} ${time_notation}`}</a>
        )
    }    
    start(){
        var time = this.runclock();
        return(
            time
        )
    }
    render(){
        return (            
            <div>
                {this.state.time>0 ?
                    <div style={{display:"flex",flexDirection:"row",justifyContent:"space-between"}}> 
                        <div style={{marginLeft:"10px"}}>
                            <h2>
                                sekarang jam : {this.start()}         
                            </h2>                            
                        </div>
                        <div style={{marginRight:"10px"}}>
                            <h2>hitung mundur :  {this.state.time}</h2>  
                        </div>

                    </div>                
                :
                null}



            </div>
        )
    }
}

export default Tugas11;

