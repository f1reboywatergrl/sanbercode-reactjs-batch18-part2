import React, {useState, useEffect} from 'react';
import axios from 'axios';

const Example = (props) => {
  
  const tabelHargaBuah = [
    ['Semangka',10000 ,'1 kg'] ,
    [ 'Anggur', 40000, '0.4 kg'],
    [ 'Strawberry', 30000, '0.5 kg'],
    [ 'Jeruk', 30000, '1 kg'],
    [ 'Mangga', 30000, '0.5 kg']                  
]
const [tabel, setTabelHargaBuah] = useState(tabelHargaBuah);
const [openForm, setOpenForm] = useState(-1);

const DeleteBaris = (event) => {
  axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${event.target.value}`)
  .then(res => {
    console.log(res);
    console.log(res.data);
  })
  event.preventDefault();
  var temp = tabelHargaBuah.filter(function(value, index) {
          return index!=event.target.value;
      });
  setTabelHargaBuah(temp)
}
useEffect( () => {
  setTabelHargaBuah(props.tabel)
}, [])
  console.log(props)

const EditBaris = (event) => {
  event.preventDefault();
  console.log(event.target.value)
  setOpenForm(event.target.value)
  axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {tabel})
  .then(res => {
      setTabelHargaBuah(res.data)
      console.log(res.data)
  })
  .catch(res => {
    console.log(res.data)
  })
}

  return (
    <div>
      <h1>Tabel Harga Buah</h1>
          <table class="tableborder">
            <thead>
              <tr>
                <th class="border header name">Nama</th>
                <th class="border header price">Harga</th>
                <th class="border header price">Berat</th>
                <th class="border header price">Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  tabelHargaBuah.map((val, index)=>{
                    
                    return(                    
                      <tr>
                        
                        <td class="border elements">{val[0]}</td>
                        <td class="border elements">{val[1]}</td>
                        <td class="border elements">{val[2]}</td>
                        <td>
                            <form>
                                <button value={index} onClick={(ev) => EditBaris(ev)}>Edit</button>
                                <button value={index} onClick={(ev) => DeleteBaris (ev)}>Delete</button>
                                {(openForm==index)? 
                                <div>
                                        <input type="text" value={val[0]}>
                                        
                                        </input>
                                        <input type="text" value={val[1]}>
                                        
                                        </input>
                                        <input type="text" value={val[2]}>
                                        
                                        </input>
                                        <form>
                                            <button value={index} onClick={(ev) => EditBaris (ev)}>Update</button>
                                        </form>
                                </div>        
                                :""}                              
                            </form>                            
                        </td>
                      </tr>
                      
                    )
                  })
                  
                }
            </tbody>
          </table>
    </div>
  );
}
Example.getInitialProps = async function(){
  const res = await fetch("http://backendexample.sanbercloud.com/api/fruits");
  const table = await res.json();
  return{
      tabel : table 
  }
}
export default Example;