import React, {useState, useEffect, useContext} from 'react';
import axios from 'axios';
import {Context} from "./main.js"

function Tugas() {
const [tabelHargaBuah, setTabel] = useContext(Context);  
const [openForm, setOpenForm] = useState(-1);
console.log(tabelHargaBuah)

const DeleteBaris = (event) => {
  event.preventDefault();
  var temp = tabelHargaBuah.filter(function(value, index) {
          return index!=event.target.value;
      });
  setTabel(temp)
}

const EditBaris = (event) => {
  event.preventDefault();
  setOpenForm(event.target.value)
}

  return (
    <div>
      <h1>Tabel Harga Buah</h1>
          <table class="tableborder">
            <thead>
              <tr>
                <th class="border header name">Nama</th>
                <th class="border header price">Harga</th>
                <th class="border header price">Berat</th>
                <th class="border header price">Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  tabelHargaBuah.map((val, index)=>{
                    
                    return(                    
                      <tr>
                        
                        <td class="border elements">{val[0]}</td>
                        <td class="border elements">{val[1]}</td>
                        <td class="border elements">{val[2]}</td>
                        <td>
                            <form>
                                <button value={index} onClick={(ev) => EditBaris(ev)}>Edit</button>
                                <button value={index} onClick={(ev) => DeleteBaris (ev)}>Delete</button>
                                {(openForm==index)? 
                                <div>
                                        <input type="text" value={val[0]}>
                                        
                                        </input>
                                        <input type="text" value={val[1]}>
                                        
                                        </input>
                                        <input type="text" value={val[2]}>
                                        
                                        </input>
                                        <form>
                                            <button value={index} onClick={(ev) => EditBaris (ev)}>Update</button>
                                        </form>
                                </div>        
                                :""}                              
                            </form>                            
                        </td>
                      </tr>
                      
                    )
                  })
                  
                }
            </tbody>
          </table>
    </div>
  );
}
Tugas.getInitialProps = async function(){
  const res = await fetch("http://backendexample.sanbercloud.com/api/fruits");
  const table = await res.json();
  return{
      tabel : table 
  }
}
export default Tugas;