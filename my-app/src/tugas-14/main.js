import React, { useState, createContext } from "react";

export const Context = createContext();

export const Provider = props => {
  const [tabelHargaBuah, setTabel] = useState([
    ['Semangka',10000 ,'1 kg'] ,
    [ 'Anggur', 40000, '0.4 kg'],
    [ 'Strawberry', 30000, '0.5 kg'],
    [ 'Jeruk', 30000, '1 kg'],
    [ 'Mangga', 30000, '0.5 kg']                  
]);
  return (
    <Context.Provider value={[tabelHargaBuah, setTabel]}>
      {props.children}
    </Context.Provider>
  );
};