import React, {Component} from 'react';

class Tugas9 extends React.Component{
    render(){
        return (
        <div className="App">
            <div>
            <div class="grid-container">
                <h2 class="item0">Form Pembelian Buah</h2>        
            </div>

            <div class="grid-container">
                <div class="item1">Nama Pelanggan</div>
                <div class="item2 input"><input type="text"></input></div>
            </div>
            <div class="grid-container">
                <div class="item1 daftar">Daftar Item</div>
                <div class="item2">
                <div class="list-item">
                    <input type="checkbox" name="item_1"></input>
                    <label for="item_1">Semangka</label>            
                </div>
                <div class="list-item">
                    <input type="checkbox" name="item_2"></input>
                    <label for="item_2">Jeruk</label>            
                </div>
                <div class="list-item">
                    <input type="checkbox" name="item_3"></input>
                    <label for="item_3">Nanas</label>            
                </div>
                <div class="list-item">
                    <input type="checkbox" name="item_4"></input>
                    <label for="item_4">Salak</label>            
                </div>
                <div class="list-item">
                    <input type="checkbox" name="item_5"></input>
                    <label for="item_5">Anggur</label>            
                </div>
                </div>        
            </div>
            <div class="grid-container">
                <input class ="item1 tombol" type="submit" value="Kirim"></input>
            </div>
            </div>
            </div>            
        );
    }
}
export default Tugas9;