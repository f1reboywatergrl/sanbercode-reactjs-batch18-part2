import React, {Component} from 'react';
let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ];
class Name extends React.Component{
    render(){
        return this.props.nama
    }
}
class Price extends React.Component{
    render(){
        return this.props.harga
    }
}
class Weight extends React.Component{
    render(){
        return this.props.berat
    }
}
class Tugas10 extends React.Component{
    render(){
        return (
            <>
            <h2 class="tabletitle">Tabel Harga Buah</h2>
            <div>
                <table class="tableborder tablealign">
                    <tr>
                        <th class="border header name">Nama</th>
                        <th class="border header price">Harga</th>
                        <th class="border header price">Berat</th>
                    </tr>                   
                                        
            {
                dataHargaBuah.map(el=> {
                    return(
                        <tr>
                            <td class="border elements"><Name nama={el.nama}/></td>
                            <td class="border elements"><Price harga={el.harga}/></td>
                            <td class="border elements"><Weight berat={el.berat}/></td>                               
                        </tr>
                    )
                })                
            }
                </table>              
            </div>
            </>
        );
    }
}

export default Tugas10;